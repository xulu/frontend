'ues strict'

import React from 'react';
import CommentList from './CommentList';
import CommentForm from './CommentForm';
import $ from 'jquery';

const REQUEST_URL = "/app/comment.json";

class CommentBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {data: []};
    this.getComments();

    // setInterval(() => this.getComments(), 5000);
  }

  handleCommentSubmit(comment) {
    let comments = this.state.data,
        newComments = comments.concat(comment);

    this.setState({data: newComments});
  }

  getComments() {
    $.ajax({
      url: REQUEST_URL,
      dataType: 'json',
      cache: false,
      success: comments => {
        this.setState({data: comments});
      },
      error: (xhr, status, error) => {
        console.log(error);
      }
    });
  }

  render() {
    return (
      <div className="ui comments">
        <CommentList data={this.state.data} />
        <CommentForm onCommentSubmit={this.handleCommentSubmit.bind(this)} />
      </div>
      // <div className="ui info message">电视节目列表</div>
    );
  }
}


export { CommentBox as default };
